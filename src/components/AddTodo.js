import { connect } from 'react-redux';
import React from 'react';
import {addTodoAction} from '../actions/actions.js';
import {toggleStatusAction} from '../actions/actions.js';

import '../css/AddTodo.css'


let AddTodo = ({ dispatch, addTodoStatus }) => {
    let addTodoInput;

    return (
        <div className="addTodo">
            <div className="addTodo__input-wrapper">
                <form onSubmit={(e) =>{
                    e.preventDefault();
                    if (addTodoInput.value) {
                        dispatch(addTodoAction(addTodoInput.value));
                        addTodoInput.value = '';
                    }
                }}>
                    <input ref={node => {
                        addTodoInput = node;
                    }}
                           className= { !addTodoStatus ? "addTodo__input" :  "addTodo__input  addTodo__input_visible" }
                    />
                    <button type="Submit" onClick={ (e) => {
                        if ( !addTodoStatus) {
                            e.preventDefault();
                            dispatch(toggleStatusAction());
                            addTodoInput.focus();
                        } else {
                            dispatch(toggleStatusAction());
                        }
                    }}
                            className="addTodo__btn"
                    ></button>
                </form>
            </div>
        </div>
    )
};


const mapStateToProps = (state) => {
    return {
        addTodoStatus: state.addTodoStatus
    }
};


AddTodo = connect(mapStateToProps , null)(AddTodo);

export default AddTodo;