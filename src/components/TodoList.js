import React from 'react';
import Todo from './Todo.js';

import '../css/TodoList.css';

const TodoList = ({
    todos,
    onTodoClick
}) => {
    return (
        <ul className="TodoList">
            { todos.map( (todo) => {
                if (!todo.text) return false

                return <Todo
                    key = {todo.id}
                    {...todo}
                    onClick = { onTodoClick}
                />
            } )}
        </ul>
    )

};


export default TodoList;