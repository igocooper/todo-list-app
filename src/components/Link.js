import React from 'react';

const Link = ({
    active,
    children,
    onSetFilterClick
}) => {
    if ( active) {
        return <span className="Header__link Header__link_active"> {children}</span>
    }
    return (
        <a href="#" onClick={ e => {
            e.preventDefault();
            onSetFilterClick()
        }}
           className="Header__link"
        >
            {children}
        </a>
    )

};

export default Link;