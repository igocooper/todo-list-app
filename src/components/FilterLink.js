import { connect } from 'react-redux';
import React from 'react';
import Link from './Link.js';
import {setVisibilityFilterAction} from '../actions/actions.js';


const mapStateFilterLinkToProps = (state , props) => {
    return {
        active: props.filter === state.visibilityFilter
    }
};

const mapDispatchFilterLinkToProps = (dispatch , props) => {
    return {
        onSetFilterClick: () => {
            dispatch(setVisibilityFilterAction(props.filter))
        }
    };
};


export const FilterLink = connect(
    mapStateFilterLinkToProps,
    mapDispatchFilterLinkToProps)(Link);
