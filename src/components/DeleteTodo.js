import { connect } from 'react-redux';
import React from 'react';
import '../css/TodoList.css';

import {deleteTodoAction} from '../actions/actions.js';

let DeleteTodoButton = ({
    id,
    dispatch
}) => (
    <span onClick= { (event) => {
        event.stopPropagation();
        dispatch(deleteTodoAction(id))
    }}
          className="TodoList__item__btn-close"
    ></span>
);

DeleteTodoButton = connect()(DeleteTodoButton);



export default DeleteTodoButton;