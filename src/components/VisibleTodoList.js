import { connect } from 'react-redux';
import React from 'react';
import TodoList from './TodoList.js';
import { toggleTodoAction } from '../actions/actions.js';
import { getVisibleTodos } from '../utils/getVisibleTodos.js';


const mapStateTodoListToProps = (state) => {
    return {
        todos: getVisibleTodos( state.todos, state.visibilityFilter)
    }
};

const mapDispatchTodoListToProps = (dispatch) => {
    return {
        onTodoClick: (id) => {
            dispatch( toggleTodoAction(id));

        }
    }
};


const VisibleTodoList = connect(
    mapStateTodoListToProps,
    mapDispatchTodoListToProps)(TodoList);


export default VisibleTodoList;