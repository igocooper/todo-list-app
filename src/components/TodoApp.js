import React from 'react';

import AddTodo from './AddTodo.js';
import VisibleTodoList from './VisibleTodoList.js';
import Header from './Header.js';

import '../css/app.css';

const TodoApp = () => {
    return (
        <div className="todoApp__wrapper">
            <Header />
            <VisibleTodoList />
            <AddTodo />
        </div>
    );
};

export default TodoApp;