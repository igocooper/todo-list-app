import React from 'react';
import DeleteTodoButton from './DeleteTodo.js';
import '../css/TodoList.css';

const Todo = ({
    onClick,
    text,
    completed,
    id,
}) => (
    <li onClick ={ () => onClick(id) }
        className={completed ? 'TodoList__item TodoList__item_completed' : 'TodoList__item'
        }
    >
        {text}
        <DeleteTodoButton id={id}  />
    </li>
);

export default Todo;