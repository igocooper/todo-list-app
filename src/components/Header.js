import React from 'react';
import { FilterLink } from './FilterLink.js';
import { getCurrentDay } from '../utils/getCurrentDay.js';

import '../css/Header.css';

const Header = () => {
    return (
        <div className="Header">
            <p className="Header__title">{ getCurrentDay() }</p>
            <p className="Header__filter">
                <FilterLink
                    filter='SHOW_ALL'
                >
                    all
                </FilterLink>
                {' '}
                <FilterLink
                    filter='SHOW_ACTIVE'
                >
                    active
                </FilterLink>
                {' '}
                <FilterLink
                    filter='SHOW_COMPLETED'
                >
                    completed
                </FilterLink>
            </p>
        </div>
    )
};

export default Header;