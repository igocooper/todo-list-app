export const getCurrentDay = () => {
    let d = new Date();
    const days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],
        currentDay = days[d.getDay()];

    return currentDay;

};