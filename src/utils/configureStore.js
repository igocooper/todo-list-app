import { applyMiddleware, createStore , compose } from 'redux';
import todoApp from '../reducers/todoApp';
import {composeWithDevTools} from 'redux-devtools-extension/developmentOnly';
import {saveState, loadState} from './localStorage.js';
import throttle from 'lodash/throttle';
import ReduxThunk from 'redux-thunk';

const configureStore = () => {

    const persistedState = loadState();
    const store = createStore(
        todoApp,
        persistedState,
        composeWithDevTools(applyMiddleware(
            ReduxThunk
        ))
    );

    store.subscribe( throttle( () => {
        saveState({
            todos: store.getState().todos
        });
    }, 1000));

    return store
};

export default configureStore;