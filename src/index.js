import React from 'react';
import ReactDOM from 'react-dom';
import Root from './components/Root.js';

import  configureStore  from './utils/configureStore.js';

//-------------------- Render VIEW WITH REACT --------------------

const store = configureStore();

ReactDOM.render(
   <Root store= { store }/>,
    document.getElementById('root') );


