const addTodoStatus = ( state = false , action) => {
    switch (action.type){
        case 'TOGGLE_STATUS':
            return !state;
        default:
            return state
    }
};


export default addTodoStatus;