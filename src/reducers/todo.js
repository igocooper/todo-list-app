
const todo = ( state = {}, action ) => {
    switch (action.type) {
        case 'ADD_TODO':
            return {
                text: action.text,
                id: action.id,
                completed: false
            };
        case 'TOGGLE_TODO':
            if ( action.id !== state.id ) {
                return state;
            }
            return {
                ...state,
                completed: !state.completed
            };
        case 'DELETE_TODO':
            if( action.id !== state.id){
                return true
            }
            return false;
        default :
            return state
    }
};

export default todo;