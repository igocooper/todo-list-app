import {combineReducers} from 'redux';
import todos from './todos';
import addTodoStatus from './addTodoStatus';
import visibilityFilter from './visibilityFilter.js';

const todoApp = combineReducers({
    todos,
    visibilityFilter,
    addTodoStatus
});

export default todoApp;