import { v4 } from 'node-uuid';

export const toggleTodoAction = (id) => {
    return {
        type: 'TOGGLE_TODO',
        id

    }
};


export const addTodoAction = (inputText) => {
    return {
        type: 'ADD_TODO',
        id: v4(),
        text: inputText

    }
};

export const toggleStatusAction = () => {
    return {
        type: 'TOGGLE_STATUS',
    }
};


export const deleteTodoAction = (id) => {
    return {
        type: 'DELETE_TODO',
        id
    }
};


export const setVisibilityFilterAction = (filter) => {
    return {
        type: 'SET_VISIBILITY_FILTER',
        filter

    }
};




