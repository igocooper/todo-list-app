FROM node:8.9.4

RUN useradd --user-group --create-home --shell /bin/false app &&\
  npm install --global npm@5.6.0

ENV HOME=/home/app

COPY package.json $HOME/todo-list-app/
RUN chown -R app:app $HOME/*

USER app
WORKDIR $HOME/todo-list-app

RUN npm install
