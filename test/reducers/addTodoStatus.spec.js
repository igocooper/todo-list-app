import {expect} from 'chai';
import reducer from  '../../src/reducers/addTodoStatus.js';

describe('addTodoStatus Reducer', () => {
    var stateBefore = false,
        action,
        stateAfter;

    describe('Handling default case', () => {
        it(`Should return initial state`, () => {
            stateAfter = reducer(stateBefore, {});

            expect(stateAfter).to.deep.equal(false);
        });
    });

    describe('Handling TOGGLE_STATUS', () => {
        beforeEach( () => {
            action = {
                type: 'TOGGLE_STATUS'
            };
            stateAfter = reducer(stateBefore, action);
        });

        it(`Should toggle state`, () => {
            expect(stateAfter).to.equal(true);
        });
    });

});
