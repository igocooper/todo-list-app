import {expect} from 'chai';
import deepFreeze from 'deep-freeze';
import reducer from  '../../src/reducers/todos.js';

describe('Todos Reducer', () => {
    var stateBefore = [],
        action,
        stateAfter;

    //deepFreeze state to make sure it hasn't been changed
    deepFreeze(stateBefore);

    describe('Handling default case', () => {
        it(`Should return initial state`, () => {
            stateAfter = reducer(stateBefore, {});

            expect(stateAfter).to.deep.equal([]);
        });
    });

    describe('Handling ADD_TODO', () => {
        beforeEach( () => {
            action = {
                type: 'ADD_TODO',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                text: 'Learn Redux'
            };
            stateAfter = reducer(stateBefore, action);
        });

        it(`Should add todo to todos array: \n\t - text should equal action.text \n\t - id should be unique id \n\t - completed should be FALSE`, () => {
            var expectedTodo = {
                text: 'Learn Redux',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                completed: false
            };
            expect(stateAfter).to.deep.include(expectedTodo);
        });
    });

    describe('Handling TOGGLE_TODO', () => {
        beforeEach( () => {
            action = {
                type: 'TOGGLE_TODO',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
            };
            stateBefore = [...stateBefore];
            stateBefore = [
                {
                    text: 'Learn Redux',
                    id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                    completed: false
                }
            ];
            deepFreeze(stateBefore);
            stateAfter = reducer(stateBefore, action);
        });

        it(`Should toggle corresponding todo.completed status:`, () => {
            expect(stateAfter[0].completed).to.equal(true);
        });
    });

    describe('Handling DELETE_TODO', () => {
        beforeEach( () => {
            action = {
                type: 'DELETE_TODO',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
            };
            stateBefore = [...stateBefore];
            stateBefore = [
                {
                    text: 'Learn Redux',
                    id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                    completed: false
                }
            ];
            deepFreeze(stateBefore);
            stateAfter = reducer(stateBefore, action);
        });

        it(`Should remove corresponding todo from todos array:`, () => {
            var removedTodo = {
                text: 'Learn Redux',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                completed: false
            };
            expect(stateAfter).to.not.deep.include(removedTodo);
        });
    });

});
