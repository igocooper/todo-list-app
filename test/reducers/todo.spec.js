import {expect} from 'chai';
import deepFreeze from 'deep-freeze';
import reducer from  '../../src/reducers/todo.js';

describe('Todo Reducer', () => {
    var stateBefore = {},
        action,
        stateAfter;

    //deepFreeze state to make sure it hasn't been changed
    deepFreeze(stateBefore);

    describe('Handling default case', () => {
        it(`Should return initial state`, () => {
            stateAfter = reducer(stateBefore, {});

            expect(stateAfter).to.deep.equal({});
        });
    });

    describe('Handling ADD_TODO', () => {
        beforeEach( () => {
            action = {
                type: 'ADD_TODO',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                text: 'Learn Redux'
            }
        });

        it(`Should return todo: \n\t - text should equal action.text \n\t - id should be unique id \n\t - completed should be FALSE`, () => {
            stateAfter = reducer(stateBefore, action);
            var todo = {
                text: 'Learn Redux',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                completed: false
            };

            expect(stateAfter).to.deep.equal(todo);
        });
    });

    describe('Handling TOGGLE_TODO', () => {
        beforeEach( () => {
            action = {
                type: 'TOGGLE_TODO',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41'
            };

            stateBefore = {
                text: 'test2',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                completed: false
            };

            stateAfter = reducer(stateBefore, action);
        });

        it(`Should toggle state.completed status`, () => {
            expect(stateAfter.completed).to.equal(true);
        });

        it(`Should return state if action.id is not equal state.id`, () => {
            action = {
                type: 'TOGGLE_TODO',
                id: 'not in state id'
            };
            stateAfter = reducer(stateBefore, action);
            expect(stateAfter).to.deep.equal(stateBefore);
        });
    });

    describe('Handling DELETE_TODO', () => {
        beforeEach( () => {
            action = {
                type: 'DELETE_TODO',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41'
            };

            stateBefore = {
                text: 'test2',
                id: 'a04fd6dd-98e5-4d6d-a707-213ff5a5ef41',
                completed: false
            };

            stateAfter = reducer(stateBefore, action);
        });

        it(`Should return FALSE if action.id is equal state.id`, () => {
            expect(stateAfter).to.equal(false);
        });

        it(`Should return TRUE if action.id is not equal state.id`, () => {
            action = {
                type: 'DELETE_TODO',
                id: 'not in state id'
            };
            stateAfter = reducer(stateBefore, action);
            expect(stateAfter).to.equal(true);
        });
    });

});
