import {expect} from 'chai';
import reducer from  '../../src/reducers/visibilityFilter.js';

describe('visibilityFilter Reducer', () => {
    var stateBefore = 'SHOW_ALL',
        action,
        stateAfter;

    describe('Handling default case', () => {
        it(`Should return initial state`, () => {
            stateAfter = reducer(stateBefore, {});

            expect(stateAfter).to.deep.equal(stateBefore);
        });
    });

    describe('Handling SET_VISIBILITY_FILTER', () => {
        beforeEach( () => {
            action = {
                type: 'SET_VISIBILITY_FILTER',
                filter: 'SHOW_ACTIVE'
            };
            stateAfter = reducer(stateBefore, action);
        });

        it(`Should set state to action.filter`, () => {
            expect(stateAfter).to.equal('SHOW_ACTIVE');
        });
    });

});
